require 'rails_helper'

RSpec.describe Tweet, type: :model do
  it 'is invalid when body lenght is bigger than 180 chars' do
    user = User.new

    tweet = Tweet.new(body: 'a'*181, user: user)

    expect(tweet).to be_invalid
  end

  it 'is valid when body lenght is smaller than 180 chars' do
    user = User.new

    tweet = Tweet.new(body: 'a'*179, user: user)

    expect(tweet).to be_valid
  end

  it 'is invalid if there is another tweet with the same body created within 24 hours' do
    user = User.new(username: 'test_user', email: 'test@test.com', password: '123456')
    user.save!
    Tweet.new(body: 'a'*170, user: user).save

    second_tweet = Tweet.new(body: 'a'*170, user: user)

    expect(second_tweet).to be_invalid
  end
end
