class Tweet < ApplicationRecord
  #validates :body, presence: true, length: { maximum: 180 }
  #validate :unique_tweet_within_24_hours

  def unique_tweet_within_24_hours
    replicated_tweets = Tweet.find_by(body: body, user: user, created_at: 24.hours.ago..Time.now)
    errors.add(
      :body,
      "You can't post a tweet with the same body within 24 hours"
    ) unless replicated_tweets.nil?
  end

  belongs_to :user
end
